/**
 * $File: util.rs $
 * $Date: 2024-04-03 00:40:54 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2024 by Shen, Jen-Chieh $
 */

pub mod random;
