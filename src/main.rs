/**
 * $File: main.rs $
 * $Date: 2024-04-01 00:48:29 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2024 by Shen, Jen-Chieh $
 */
use std::env;
use std::io;
use std::io::Write;

mod dna;
mod population;
mod util;

const DEFAULT_PHRASE: &str = "To be or not to be";

/// Return the phrase by user's choice'.
fn get_phrase() -> io::Result<String> {
    print!("[?] Enter your phrase: ({}) ", DEFAULT_PHRASE);
    let _ = io::stdout().flush();
    let mut phrase = String::new();
    let stdin = io::stdin();
    stdin.read_line(&mut phrase)?;

    let trimmed = phrase.trim().to_string(); // make a copy

    if trimmed == "" {
        Ok(DEFAULT_PHRASE.to_owned()) // Return default phrase
    } else {
        Ok(trimmed)
    }
}

///  Start envolving the humanity!
///
/// # Arguments
///
/// * `phrase` - The target phrase
fn start(phrase: &String) {
    // Init population, ready to start envolving!
    let mutation_rate = 0.01;
    let mut population = population::Population::new(&phrase, mutation_rate, 200);

    loop {
        population.natural_selection();
        population.generate();
        population.calc_fitness();
        population.evaluate();

        population.display_info();

        if population.finished {
            break;
        }
    }
}

///  Program Entry
fn main() {
    env::set_var("RUST_BACKTRACE", "1");

    let phrase: String = get_phrase().unwrap();
    println!("Your phrase: {}", phrase);

    start(&phrase);
}
