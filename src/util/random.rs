/**
 * $File: random.rs $
 * $Date: 2024-04-03 15:34:42 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2024 by Shen, Jen-Chieh $
 */
use rand::Rng;
use std::ops::Range;

/// Handy random function.
///
/// # Arguments
///
/// * `range` - Definition of min and max range.
#[allow(dead_code)]
pub fn float(range: Range<f32>) -> f32 {
    rand::thread_rng().gen_range(range)
}

/// Handy random function.
///
/// # Arguments
///
/// * `range` - Definition of min and max range.
pub fn int(range: Range<i32>) -> i32 {
    rand::thread_rng().gen_range(range)
}
