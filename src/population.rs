/**
 * $File: population.rs $
 * $Date: 2024-04-01 17:11:53 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2024 by Shen, Jen-Chieh $
 */
use crate::dna;
use crate::util::random;

const PERFECT_SCORE: f32 = 1.0;

///  Map the range in different ratio.
///
/// # Examples
///
/// * map(50, 100, 1) -> 0.5
/// * map(0.5, 1, 100) -> 50
///
/// # Arguments
///
/// * `val` - Dynamic value should change
/// * `r1` - From max value
/// * `r2` - To max vlue
fn map(val: f32, r1: f32, r2: f32) -> f32 {
    val * (r2 / r1)
}

pub struct Population {
    generation: i32,            // Generation count
    humans: Vec<dna::DNA>,      // People in this population
    mating_pool: Vec<dna::DNA>, // We store elite in this pool for next generation
    mutation_rate: f32,
    pub finished: bool, // Are we finished envolving
    phrase: String,     // Target phrase we wished to envolve to!

    best_phrase: String,
}

impl Population {
    pub fn new(p: &String, mr: f32, num: i32) -> Self {
        let mut population = Self {
            generation: 0,
            phrase: p.clone(),
            humans: Self::init_humans(p, num),
            mating_pool: Vec::new(),
            mutation_rate: mr,
            finished: false,
            best_phrase: "".to_string(),
        };
        population.calc_fitness();
        population
    }

    /// Called it once; generate the first generation of human.
    ///
    /// # Arguments
    ///
    /// * `phrase` - Target phrase used for calculate the needed humans (DNA).
    fn init_humans(phrase: &String, num: i32) -> Vec<dna::DNA> {
        let mut humans = Vec::new();
        for _ in 0..num {
            let agent = dna::DNA::new(phrase.len());
            humans.push(agent);
        }
        humans
    }

    ///  Score them.
    pub fn calc_fitness(&mut self) {
        for agent in self.humans.iter_mut() {
            agent.calc_fitness(&self.phrase);
        }
    }

    /// Generate a mating pool
    pub fn natural_selection(&mut self) {
        self.mating_pool.clear(); // clean up before use

        let mut max_fitness: f32 = 0.0;
        for i in 0..self.humans.len() {
            if self.humans[i].fitness > max_fitness {
                max_fitness = self.humans[i].fitness;
            }
        }

        for i in 0..self.humans.len() {
            let fitness: f32 = map(self.humans[i].fitness, max_fitness, 1.0);
            let n: i32 = (fitness * 100.0).floor() as i32;
            for _ in 0..n {
                self.mating_pool.push(self.humans[i].clone());
            }
        }
    }

    /// Create a new generation
    pub fn generate(&mut self) {
        let mating_pool_len: i32 = self.mating_pool.len() as i32;
        for i in 0..self.humans.len() {
            let a = random::int(0..mating_pool_len);
            let b = random::int(0..mating_pool_len);
            let partner_a = &self.mating_pool[a as usize];
            let partner_b = &self.mating_pool[b as usize];
            let mut child: dna::DNA = partner_a.crossover(partner_b);
            child.mutate(self.mutation_rate);
            self.humans[i] = child;
        }
        self.generation += 1;
    }

    // Compute the current "most fit" member of the population
    pub fn evaluate(&mut self) {
        let mut world_record = 0.0;
        let mut index = 0;
        for i in 0..self.humans.len() {
            if self.humans[i].fitness > world_record {
                world_record = self.humans[i].fitness;
                index = i;
            }
        }

        self.best_phrase = self.humans[index].get_phrase();
        if world_record == PERFECT_SCORE {
            self.finished = true;
        }
    }

    ///  Display population info
    pub fn display_info(&self) {
        println!("{}: {}", self.generation, self.best_phrase);
    }
}
