/**
 * $File: dna.rs $
 * $Date: 2024-04-01 01:05:01 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2024 by Shen, Jen-Chieh $
 */
use rand::Rng;

/// Generate a new character
fn new_char() -> char {
    let mut c: u8 = rand::thread_rng().gen_range(63..122);
    if c == 63 {
        c = 32;
    }
    if c == 64 {
        c = 46;
    }

    c as char
}

#[derive(Clone)]
pub struct DNA {
    genes: Vec<char>,
    pub fitness: f32,
}

impl DNA {
    pub fn new(num: usize) -> Self {
        Self {
            genes: Self::init_genes(num),
            fitness: 0.0,
        }
    }

    fn init_genes(num: usize) -> Vec<char> {
        let mut new_genes = Vec::new();

        for _ in 0..num {
            let c: char = new_char();
            new_genes.push(c);
        }

        new_genes
    }

    /// Return the phrase represent by genes
    pub fn get_phrase(&self) -> String {
        self.genes.iter().collect()
    }

    /// Higher fitness the better the DNA is.
    pub fn calc_fitness(&mut self, target: &String) {
        let mut score: f32 = 0.0;
        for i in 0..self.genes.len() {
            let target_ch: char = target.to_string().chars().nth(i).unwrap();
            // If the character matches, we add 1 to the score.
            if self.genes[i] == target_ch {
                score += 1.0;
            }
        }
        let len: f32 = target.len() as f32;
        self.fitness = score / len;
    }

    // Crossover with another DNA
    pub fn crossover(&self, partner: &DNA) -> DNA {
        let genes_len = self.genes.len();

        // Born a new child
        let mut child = DNA::new(genes_len);

        // Midpoint to mix our genes!
        let midpoint = rand::thread_rng().gen_range(0..genes_len);

        // Half is from this DNA, another half is from our partner!
        for i in 0..genes_len {
            if i > midpoint {
                child.genes[i] = self.genes[i];
            } else {
                child.genes[i] = partner.genes[i];
            }
        }

        child
    }

    /// Mutate is used to brake DNA limiation!
    pub fn mutate(&mut self, mutate_rate: f32) {
        for i in 0..self.genes.len() {
            if rand::thread_rng().gen_range(0.0..1.0) < mutate_rate {
                self.genes[i] = new_char();
            }
        }
    }
}
