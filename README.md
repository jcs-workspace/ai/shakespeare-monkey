# shakespeare-monkey 

First try on [Genetic Algorithm](https://en.wikipedia.org/wiki/Genetic_algorithm).

![](./etc/demo.gif)

The code is red very similar to the tutorial sketch code in
[NOC 9.4 Genetic Algorithm - Shakespeare by codingtrain](https://editor.p5js.org/codingtrain/sketches/PqRSmKLQU).


## 📌 References

- [9.3: Genetic Algorithm: Shakespeare Monkey Example - The Nature of Code](https://www.youtube.com/watch?v=nrKjSeoc7fc&list=PLRqwX-V7Uu6bJM3VgzjNV5YxVxUwzALHV&index=3)
- [Infinite monkey theorem](https://en.wikipedia.org/wiki/Infinite_monkey_theorem)
